<?php /*/pages/profile.locality.php — выбор региона юзера
Переработаная, облегченная и адаптированая под последнюю (на текущий момент) версию DCMS. 
ПРОДАВАТЬ ДАННЫЙ ФАЙЛ (самостоятельно или в архивах) КАТЕГОРИЧЕСКИ ЗАПРЕЩЕНО! Исключения: 
даный файл может быть частью комплексного заказа, выполнямого мной (S1S13AF7) */
include_once '../sys/inc/start.php'; // подключаем ядро системы
$ank = (empty($_GET['id'])) ? $user:new user((int) $_GET['id']);
$doc = new document($user->id === $ank->id ? 1:4); /* +admins */
$doc->title = __('Мой регион'); // Заголовок страницы
if(!$ank->group)
    $doc->access_denied(__('Нет данных'));
if ($ank->group >= $user->group && $ank->id != $user->id)
    $doc->access_denied(__('Доступ к данной странице запрещен'));

if (isset($_POST ['save'])) {

    if(!empty($_POST['country']) && $_POST['country'] != $ank->country) {

    $c = $db->prepare("SELECT * FROM `countries` WHERE `code` = ?");
    $c->execute(Array(text::input_text($_POST['country'])));

        if($country = $c->fetch()) {
           $ank->region_c = 0; #fix
           $user->country = $country['code'];
        } else $doc->err(__('Country not found')); }

    if(!empty($_POST['region']) && $ank->country && (int) $_POST['region'] != $ank->region_c) {

    $r = $db->prepare("SELECT * FROM `regions` WHERE `id` = ? AND  `country` = ?");
    $r->execute(Array((int) $_POST['region'], $ank->country));

        if($region = $r->fetch()) {
           $ank->region_c = $region['id'];
           $ank->region_t = $region['name'];
        } else $doc->err(__('This Region has not been found for Your Country')); }

}

$form = new design();
$form->assign('method', 'post');
$form->assign('action', '?' . passgen());
$elements = array();
$options = array();
$countries = $db->query("SELECT * FROM `countries` ORDER BY `english_n` ASC;");
$u_country = ($ank->country)? $ank->country:"UA"; /* країна користувача */
while ($country = $countries->fetch())
$options [] = array($country['code'], ($country['code'] == $user->country ? $country['country_n'] : $country['english_n']), $country['code'] == $u_country);
$elements[] = array('type' => 'select', 'br' => 1, 'title' => __('Страна'), 'info' => array('name' => 'country', 'options' => $options, 'br' => 1));

if ($ank->country) {
$r = $db->prepare("SELECT * FROM `regions` WHERE `country` = ?");
$r ->execute(array($ank->country));
if ($r->rowCount()) {
while ($region = $r->fetch())
 $regions[] = array($region['id'], $region['name'], $region['id'] == $ank->region_c || $region['name'] == $ank->region_t);
$elements[] = array('type' => 'select', 'br' => 1, 'title' => __('Область'), 'info' => array('name' => 'region', 'options' => $regions, 'br' => 1));
 } else $doc->err(__('For your country did not match any region'));
} // Область/штат/край...

$elements[] = array('type' => 'submit', 'br' => 0, 'info' => array('name' => 'save', 'value' => __('Применить'))); // кнопка
$form->assign('el', $elements);
$form->display('input.form.tpl');
$doc->ret(__('Личное меню'), '/menu.user.php');
?>